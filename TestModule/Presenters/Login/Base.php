<?php

namespace TestModule\Presenters\Login;

use TestModule\Presenters\RootPresenter;

class Base extends RootPresenter
{

    function needLogin()
    {
        return false;
    }

    function login(){
        $this->template = "login.tpl";
    }
    function logout(){
        $this->user->logout();
        $this->redirectToLink("AdminLogin");
    }
}