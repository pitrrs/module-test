<?php
/**
 * Created by PhpStorm.
 * User: pitrr
 * Date: 28.12.2015
 * Time: 18:20
 */

namespace TestModule\Presenters;


use PKRS\SPresenter;

abstract class RootPresenter extends SPresenter
{
    function needLogin()
    {
        return false;
    }

    function setTheme()
    {

        $this->smarty->assign("THM", dirname(dirname(__FILE__)).DS."Themes".DS."Classic".DS);
        $this->template = "dynamic.tpl";
        return dirname(dirname(__FILE__)).DS."Themes".DS."SbAdmin";
    }
}