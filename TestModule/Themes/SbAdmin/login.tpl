{extends file="_layout.tpl"}
{block name="content"}
    <div class="col-md-6 col-md-offset-3">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">Login</h3>
            </div>
            <div class="panel-body">
                <form method="post" action="{a}Admin{/a}">
                    <input type="hidden" name="action" value="user_login">
                    <div class="form-group">
                        <label>Uživatel:</label>
                        <input type="text" name="login" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>Heslo:</label>
                        <input type="password" name="pass" class="form-control">
                    </div>
                    <input type="submit" value="Login" class="btn btn-primary">
                </form>
            </div>
        </div>
    </div>
{/block}