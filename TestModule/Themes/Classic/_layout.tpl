<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>{$admin_name}</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
</head>
<body>

<nav class="navbar navbar-default">
  <div class="container">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="{a}Admin{/a}">{$admin_name}</a>
    </div>

      <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
    {if $user}
      <ul class="nav navbar-nav navbar-right">
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">{$user.login} <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="#">Nastavení</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="{a}AdminLogout{/a}">Odhlásit</a></li>
          </ul>
        </li>
      </ul>
    {/if}
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>

    <div class="container">
        {if $messages}
            <div class="row">
            {foreach from=$messages item=m key=t}
                <div class="col-md-12 alert alert-{if $t=='ok'}success{elseif $t=='err'}danger{else}info{/if}">{$m}</div>
            {/foreach}
        </div>
        {/if}
        <div class="row">

            <div class="col-md-12">
                {block name="content"}{/block}
            </div>
        </div>
    </div>
</body>
</html>