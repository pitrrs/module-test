<?php
namespace TestModule;

class TestModule extends \PKRS\Core\Modules\ExternalModule {

    var $config;

    public function __construct(){
        if (!class_exists("\\PKRS\\Core\\Modules\\ExternalModule"))
            die("Required PKRS Framework > 0.2.0");
        parent::__construct();
    }

    public function run($config){
        $this->config = self::gc()->getConfig()->getConfGroup("AdminModule");
        foreach($config as $k=>$v){
            $this->config[$k] = $v;
        }
        $r = self::gc()->getRouter();

        $r->map("*",$this->config["rewrite"]."/login",["ns"=> "/TestModule/Presenters/Login", "c"=>"Base","a"=>"login"],"AdminLogin");
        $r->map("*",$this->config["rewrite"]."/logout",["ns"=> "/TestModule/Presenters/Login", "c"=>"Base","a"=>"logout"], "AdminLogout");

        if (is_callable($this->config["router"]))
            call_user_func_array($this->config["router"],[$r, $this->config["rewrite"]]);
        else die($this->config["router"]." is not callable!");

        $v = self::gc()->getView()->smarty();
        $v->assign("admin_name", $this->config["admin_name"]);
        $v->assign("AdminConf", $this->config);
        $v->assign("layout", dirname(__FILE__).DS."Themes".DS."SbAdmin".DS."_layout.tpl");
    }

}